; $Id$

The purpose of this module is to provide a built-in way of adding a location
context to searches/views of results.

It supports several ways for users to choose a location, as well as an API for
other modules to set the search location. It supports anonymous preferences
through a cookie, and stores user preferences either using location_user or its
own profile fields.

To make use of the user's preferred search, this module provides a views field,
filter, and argument handlers to make it easier to build location-aware results
screens.

It also modifies the core Drupal search to add the specified location settings,
and implements an external search hook to modify whitepages searches.
